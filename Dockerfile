FROM httpd:2.4
COPY conf.d /usr/local/apache2/conf/reverse
RUN echo "Include conf/reverse/*.conf" >> /usr/local/apache2/conf/httpd.conf
RUN sed -i \
        -e 's/^#\(LoadModule .*mod_proxy.so\)/\1/' \
        -e 's/^#\(LoadModule .*mod_proxy_http.so\)/\1/' \
        conf/httpd.conf
